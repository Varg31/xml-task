package com.shapravsky;

import com.shapravsky.model.Gun;
import com.shapravsky.reader.StAXReader;
import com.shapravsky.utils.ExtensionChecker;
import com.shapravsky.utils.GunComparator;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class Parser {
    public static void main(String[] args) {
        File xml = new File("src/main/resources/GunsXML.xml");
        File xsd = new File("src/main/resources/XMLSchema.xsd");

        if (checkIfXML(xml) && checkIfXSD(xsd)) {
//            StAX
            printList(StAXReader.parseGuns(xml, xsd), "StAX");
        }
    }

    private static boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

    private static void printList(List<Gun> guns, String parserName) {
        Collections.sort(guns, new GunComparator());
        System.out.println(parserName);
        for (Gun gun : guns) {
            System.out.println(gun);
            System.out.println();
        }
    }
}
