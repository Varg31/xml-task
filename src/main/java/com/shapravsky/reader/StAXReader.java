package com.shapravsky.reader;

import com.shapravsky.model.Gun;
import com.shapravsky.model.TacticTechChar;

import javax.xml.stream.*;
import javax.xml.stream.events.*;
import java.io.*;
import java.util.*;

public class StAXReader {
    private static List<Gun> gunsList = new ArrayList<>();

    public static List<Gun> parseGuns(File xml, File xsd){
        Gun gun = null;
        TacticTechChar tacticTechChar = null;// = new TacticTechChar();
        List<String> materialsList = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "gun":
                            gun = new Gun();
                        case "model":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gun != null;
                            gun.setModel(xmlEvent.asCharacters().getData());
                            break;
                        case "handy":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gun != null;
                            gun.setHandy(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "origin":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert gun != null;
                            gun.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        case "TTC":
                            xmlEvent = xmlEventReader.nextEvent();
                            tacticTechChar = new TacticTechChar();
                            gun.setTechChar(tacticTechChar);
                            break;
                        case "range":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert tacticTechChar != null;
                            tacticTechChar.setRange(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "sightRange":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert tacticTechChar != null;
                            tacticTechChar.setSightRange(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "clip":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert tacticTechChar != null;
                            tacticTechChar.setClip(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "optics":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert tacticTechChar != null;
                            tacticTechChar.setOptics(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "material":
                            String[] split = null;

                            xmlEvent = xmlEventReader.nextEvent();
                            split = (xmlEvent.asCharacters().getData()).split(", ");

                            assert gun != null;
                            materialsList = Arrays.asList(split);
                            gun.setMaterial(materialsList);
                            break;
                    }
                }
                    if (xmlEvent.isEndElement()) {
                        EndElement endElement = xmlEvent.asEndElement();
                        if (endElement.getName().getLocalPart().equals("gun")) {
                            gunsList.add(gun);
                        }
                    }
                }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return gunsList;
    }
}
