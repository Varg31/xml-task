package com.shapravsky.model;

public class TacticTechChar {
    private int range;
    private int sightRange;
    private boolean clip;
    private boolean optics;

    /**
     * Constructors
     */
    public TacticTechChar() {}

    public TacticTechChar(int range, int sigthRange, boolean clip, boolean optics) {
        this.range = range;
        this.sightRange = sigthRange;
        this.clip = clip;
        this.optics = optics;
    }

    /**
     * Getters
     */
    public int getRange() {
        return range;
    }

    public int getSightRange() {
        return sightRange;
    }

    public boolean isClip() {
        return clip;
    }

    public boolean isOptics() {
        return optics;
    }

    /**
     * Setters
     */
    public void setRange(int range) {
        this.range = range;
    }

    public void setSightRange(int sightRange) {
        this.sightRange = sightRange;
    }

    public void setClip(boolean clip) {
        this.clip = clip;
    }

    public void setOptics(boolean optics) {
        this.optics = optics;
    }

    @Override
    public String toString() {
        return "Range: " + getRange() + "; " +
                "Sight Range: " + sightRange + "; " +
                "Clip: " + isClip() + "; " +
                "Optics: " + isOptics();
    }
}
