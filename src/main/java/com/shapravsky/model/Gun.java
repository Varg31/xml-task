package com.shapravsky.model;

import java.util.List;

public class Gun {
    private String model;
    private int handy;
    private String origin;
    private TacticTechChar techChar;
    private List<String> material;

    /**
     * Constructors
     */
    public Gun() {}

    public Gun(String model, int handy, String origin, TacticTechChar techChar, List<String> material) {
        this.model = model;
        this.handy = handy;
        this.origin = origin;
        this.techChar = techChar;
        this.material = material;
    }

    /**
     * Getters
     */
    public String getModel() {
        return model;
    }

    public int getHandy() {
        return handy;
    }

    public String getOrigin() {
        return origin;
    }

    public TacticTechChar getTechChar() {
        return techChar;
    }

    public List<String> getMaterial() {
        return material;
    }

    /**
     * Setters
     * */
    public void setModel(String model) {
        this.model = model;
    }

    public void setHandy(int handy) {
        this.handy = handy;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setTechChar(TacticTechChar techChar) {
        this.techChar = techChar;
    }

    public void setMaterial(List<String> material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Model: " + getModel() + "\n" +
                "Handy: " + getHandy() + "\n" +
                "Origin: " + getOrigin() + "\n" +
                "Tactical and technical characteristic:" +  "\t" +techChar.toString() + "\n" +
                "Material: " + getMaterial();
    }
}
