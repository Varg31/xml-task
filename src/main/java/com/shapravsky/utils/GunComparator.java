package com.shapravsky.utils;

import com.shapravsky.model.Gun;

import java.util.Comparator;

public class GunComparator implements Comparator<Gun> {
    @Override
    public int compare(Gun o1, Gun o2) {
        return Integer.compare(o1.getTechChar().getSightRange(), o2.getTechChar().getRange());
    }
}
